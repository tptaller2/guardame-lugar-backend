﻿
using GuardameLugar.Common.Dto;
using GuardameLugar.Common.Exceptions;
using GuardameLugar.Common.Helpers;
using GuardameLugar.Core.Dacs;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace GuardameLugar.Core
{
	public interface IGarageService
	{
		Task GarageRegister(GarageRegisterDto garageRegisterDto);
		Task<List<LocalidadesDto>> Localidades();
		Task<List<LocalidadesDto>> LocalidadesPorGarages();
		Task<GarageByIdDto> GetGarageById(int garageId);
		Task<List<GarageDto>> GetGarageByUser(int userId);
		Task<List<GarageDto>> GetGarages(string vehiculos, int? localidades);
		Task UpdateGarage(UpdateGarageDto updateGarageDto);
		Task GarageReservation(GarageReservationDto garageReservationDto);
		Task<List<ReservationUserDto>> GarageReservationByUserId(int userId);
		Task<List<ReservationGarageDto>> GarageReservationByGarageId(int garageId);
		Task UpdateReservation(UpdateReservationDto updateReservationDto);
		Task<List<GetComentDto>> ComentByGarage(int garageId);
		Task<GetComentDto> ComentByReserva(int reservationId);
		Task ComentRegister(ComentDto comentDto);

	}
	public class GarageService : IGarageService
	{
		private readonly ILogger<GarageService> _logger;
		private readonly IGuardameLugarDacService _guardameLugarDacService;

		public GarageService(ILogger<GarageService> logger, IGuardameLugarDacService guardameLugarDacService)
		{
			_logger = logger;
			_guardameLugarDacService = guardameLugarDacService;
		}

		public async Task GarageRegister(GarageRegisterDto garageRegisterDto)
		{
			try
			{
				//validacion de datos completos
				Throws.ThrowIfNull(garageRegisterDto, new BadRequestException("Los parametros son nulos."));
				Throws.ThrowIfNull(garageRegisterDto.user_id, new BadRequestException("usuario esta vacio."));
				Throws.ThrowIfNull(garageRegisterDto.altura_maxima, new BadRequestException("AlturaMaxima esta vacio."));
				Throws.ThrowIfEmpty(garageRegisterDto.coordenadas, new BadRequestException("coordenadas esta vacio."));
				Throws.ThrowIfEmpty(garageRegisterDto.direccion, new BadRequestException("direccion esta vacio."));
				Throws.ThrowIfNull(garageRegisterDto.localidad_garage, new BadRequestException("localidad garage esta vacio."));
				Throws.ThrowIfNull(garageRegisterDto.lugar_autos, new BadRequestException("lugar autos esta vacio."));
				Throws.ThrowIfNull(garageRegisterDto.lugar_bicicletas, new BadRequestException("lugar bicicletas esta vacio."));
				Throws.ThrowIfNull(garageRegisterDto.lugar_camionetas, new BadRequestException("lugar camionetas esta vacio."));
				Throws.ThrowIfNull(garageRegisterDto.lugar_motos, new BadRequestException("lugar motos esta vacio."));
				Throws.ThrowIfEmpty(garageRegisterDto.nombre_garage, new BadRequestException("nombre garage esta vacio."));
				Throws.ThrowIfEmpty(garageRegisterDto.telefono, new BadRequestException("telefono esta vacio."));

				//DataBase
				await _guardameLugarDacService.GarageRegister(garageRegisterDto);


			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}


		}

		public async Task<List<LocalidadesDto>> Localidades()
		{
			try
			{
				List<LocalidadesDto> listaLocalidades = await _guardameLugarDacService.Localidades();
				return listaLocalidades;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task<List<LocalidadesDto>> LocalidadesPorGarages()
		{
			try
			{
				List<LocalidadesDto> listaLocalidades = await _guardameLugarDacService.LocalidadesPorGarages();
				return listaLocalidades;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}
		}

		public async Task<GarageByIdDto> GetGarageById(int garageId)
		{
			try
			{
				//valido que ID no sea nulo
				Throws.ThrowIfNotPositive(garageId, new BadRequestException("ID no puede ser negativo."));

				GarageByIdDto garageByIdDto = await _guardameLugarDacService.GetGarageById(garageId);

				//valido que exista el garage
				Throws.ThrowIfNull(garageByIdDto, new NotFoundException("No se encontro el Garage."));

				return garageByIdDto;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task<List<GarageDto>> GetGarageByUser(int userId)
		{
			try
			{
				//valido que ID no sea nulo
				Throws.ThrowIfNotPositive(userId, new BadRequestException("ID no puede ser negativo."));

				List<GarageDto> garageList = await _guardameLugarDacService.GetGarageByUser(userId);

				//valido que exista el garage
				Throws.ThrowIfNull(garageList, new NotFoundException("No se encontraron Garages para el usuario."));

				return garageList;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task<List<GarageDto>> GetGarages(string vehiculos, int? localidades)
		{
			try
			{
				//Valido que parametro me ingresa para realizar la consulta
				string query;

				if (string.IsNullOrEmpty(vehiculos) && localidades == null)
				{
					query = @"select top 50 g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g
							 inner join localidades l on g.localidad_garage = l.localidad_id
							 left join reservas r on g.garage_id = r.garage_id
							 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
							 where g.lugar_autos > 0 OR lugar_camionetas > 0 OR lugar_bicicletas > 0 OR lugar_motos > 0
							 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
							 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima
							 order by g.localidad_garage;";
				}
				else if (string.IsNullOrEmpty(vehiculos) && localidades > 0)
				{
					query = @$"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g
							  inner join localidades l on g.localidad_garage = l.localidad_id
							  left join reservas r on g.garage_id = r.garage_id
							  left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
							  where g.localidad_garage = { localidades }
							  and (g.lugar_autos > 0 OR lugar_camionetas > 0 OR lugar_bicicletas > 0 OR lugar_motos > 0)
							  group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
							  g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima;";
				}
				else if (vehiculos != "" && localidades == null)
				{
					query = (vehiculos.ToLower()) switch
					{
						"autos" => @"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
							 inner join localidades l on g.localidad_garage = l.localidad_id
							 left join reservas r on g.garage_id = r.garage_id
							 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
							 where g.lugar_autos > 0
							 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
							 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima
							 order by g.localidad_garage;",

						"motos" => @"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
							 inner join localidades l on g.localidad_garage = l.localidad_id
							 left join reservas r on g.garage_id = r.garage_id
							 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
							 where g.lugar_motos > 0
							 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
							 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima
							 order by g.localidad_garage;",

						"camionetas" => @"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
							 inner join localidades l on g.localidad_garage = l.localidad_id
							 left join reservas r on g.garage_id = r.garage_id
							 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
							 where g.lugar_camionetas > 0
							 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
							 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima
							 order by g.localidad_garage;",

						"bicicletas" => @"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
							 inner join localidades l on g.localidad_garage = l.localidad_id
							 left join reservas r on g.garage_id = r.garage_id
							 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
							 where g.lugar_bicicletas > 0
							 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
							 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima
							 order by g.localidad_garage;",

						_ => throw new BadRequestException($"Ningun parametro de {vehiculos} se corresponde con un valor valido"),
					};
				}
				else
				{
					query = (vehiculos?.ToLower()) switch
					{
						"autos" => @$"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
									 inner join localidades l on g.localidad_garage = l.localidad_id
									 left join reservas r on g.garage_id = r.garage_id
									 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
									 where g.localidad_garage = {localidades} and g.lugar_autos > 0
									 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
									 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima;",

						"motos" => @$"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
									 inner join localidades l on g.localidad_garage = l.localidad_id
									 left join reservas r on g.garage_id = r.garage_id
									 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
									 where g.localidad_garage = {localidades} and g.lugar_motos > 0
									 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
									 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima;",

						"camionetas" => @$"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
										  inner join localidades l on g.localidad_garage = l.localidad_id
										  left join reservas r on g.garage_id = r.garage_id
										  left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
										  where g.localidad_garage = {localidades} and g.lugar_camionetas > 0
										  group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
										  g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima;",

						"bicicletas" => @$"select g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
										  inner join localidades l on g.localidad_garage = l.localidad_id
										  left join reservas r on g.garage_id = r.garage_id
										  left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
										  where g.localidad_garage = {localidades} and g.lugar_bicicletas > 0
										  group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
										  g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima;",

						_ => throw new BadRequestException($"Ningun parametro de {vehiculos} se corresponde con un valor valido"),
					};
				}

				List<GarageDto> garageList = await _guardameLugarDacService.GetGarages(query);

				return garageList;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task UpdateGarage(UpdateGarageDto updateGarageDto)
		{
			//Valido los campos necesarios
			Throws.ThrowIfNull(updateGarageDto, new BadRequestException("Los parametros son nulos."));
			Throws.ThrowIfNull(updateGarageDto.altura_maxima, new BadRequestException("AlturaMaxima esta vacio."));
			Throws.ThrowIfEmpty(updateGarageDto.coordenadas, new BadRequestException("coordenadas esta vacio."));
			Throws.ThrowIfEmpty(updateGarageDto.direccion, new BadRequestException("direccion esta vacio."));
			Throws.ThrowIfNull(updateGarageDto.localidad_garage, new BadRequestException("localidad garage esta vacio."));
			Throws.ThrowIfNull(updateGarageDto.lugar_autos, new BadRequestException("lugar autos esta vacio."));
			Throws.ThrowIfNull(updateGarageDto.lugar_bicicletas, new BadRequestException("lugar bicicletas esta vacio."));
			Throws.ThrowIfNull(updateGarageDto.lugar_camionetas, new BadRequestException("lugar camionetas esta vacio."));
			Throws.ThrowIfNull(updateGarageDto.lugar_camionetas, new BadRequestException("lugar camionetas esta vacio."));
			Throws.ThrowIfNull(updateGarageDto.lugar_motos, new BadRequestException("lugar motos esta vacio."));
			Throws.ThrowIfEmpty(updateGarageDto.nombre_garage, new BadRequestException("nombre garage esta vacio."));
			Throws.ThrowIfEmpty(updateGarageDto.telefono, new BadRequestException("telefono esta vacio."));
			Throws.ThrowIfNull(updateGarageDto.garage_id, new BadRequestException("garage_id esta vacio."));

			//hago el update
			await _guardameLugarDacService.UpdateGarage(updateGarageDto);
		}

		public async Task GarageReservation(GarageReservationDto garageReservationDto)
		{
			try
			{
				//Valido los campos necesarios
				Throws.ThrowIfNull(garageReservationDto, new BadRequestException("Los parametros son nulos."));
				Throws.ThrowIfNull(garageReservationDto.user_id, new BadRequestException("usuario esta vacio."));
				Throws.ThrowIfNull(garageReservationDto.garage_id, new BadRequestException("garage esta vacio."));
				Throws.ThrowIfNull(garageReservationDto.tipo_vehiculo, new BadRequestException("tipo de vehiculo esta vacio."));

				if (garageReservationDto.tipo_vehiculo != "lugar_autos" && garageReservationDto.tipo_vehiculo != "lugar_motos"
					&& garageReservationDto.tipo_vehiculo != "lugar_camionetas" && garageReservationDto.tipo_vehiculo != "lugar_bicicletas")
				{
					throw new BadRequestException("tipo de vehiculo no valido.");
				}

				//verificar lugar disponible y hacer update
				int disp = await _guardameLugarDacService.SpaceValidator(garageReservationDto.tipo_vehiculo, garageReservationDto.garage_id);

				if (disp == 0)
				{
					throw new BadRequestException("El garage no dispone de lugares para ese vehiculo.");
				}

				//actualizo el valor del campo en garages
				await _guardameLugarDacService.SpaceReservation(garageReservationDto.tipo_vehiculo, garageReservationDto.garage_id);

				//tratado variable tipo_vehiculo
				switch (garageReservationDto.tipo_vehiculo)
				{
					case "lugar_autos":
						garageReservationDto.tipo_vehiculo = "autos";
						break;
					case "lugar_motos":
						garageReservationDto.tipo_vehiculo = "motos";
						break;
					case "lugar_camionetas":
						garageReservationDto.tipo_vehiculo = "camionetas";
						break;
					case "lugar_bicicletas":
						garageReservationDto.tipo_vehiculo = "bicicletas";
						break;
					default:
						break;
				}

				//inserto la reserva
				await _guardameLugarDacService.GarageReservation(garageReservationDto);

			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task<List<ReservationUserDto>> GarageReservationByUserId(int userId)
		{
			try
			{
				//valido que ID no sea nulo
				Throws.ThrowIfNotPositive(userId, new BadRequestException("ID no puede ser negativo."));

				List<ReservationUserDto> garageList = await _guardameLugarDacService.GarageReservationByUserId(userId);

				//valido que existan reservas para el usuario
				Throws.ThrowIfNull(garageList, new NotFoundException("No se encontraron Garages para el usuario."));

				return garageList;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task<List<ReservationGarageDto>> GarageReservationByGarageId(int garageId)
		{
			try
			{
				//valido que ID no sea nulo
				Throws.ThrowIfNotPositive(garageId, new BadRequestException("ID no puede ser negativo."));

				List<ReservationGarageDto> garageList = await _guardameLugarDacService.GarageReservationByGarageId(garageId);

				//valido que existan reservas para el garage
				Throws.ThrowIfNull(garageList, new NotFoundException("No se encontraron Garages para el usuario."));

				return garageList;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task UpdateReservation(UpdateReservationDto updateReservationDto)
		{
			try
			{
				//valido que ID no sea nulo
				Throws.ThrowIfNull(updateReservationDto, new BadRequestException("El objeto es nulo."));
				Throws.ThrowIfNotPositive(updateReservationDto.reserva_id, new BadRequestException("ID no puede ser negativo."));
				Throws.ThrowIfNotPositive(updateReservationDto.estado, new BadRequestException("Estado no puede ser negativo."));
				Throws.ThrowIfNotPositive(updateReservationDto.garage_id, new BadRequestException("Id de garage no puede ser negativo."));
				Throws.ThrowIfNullOrEmpty(updateReservationDto.tipo_vehiculo, new BadRequestException("el tipo de vehiculo no puede ser nulo."));

				//seteo el tipo de vehiculo segun la tabla garage
				updateReservationDto.tipo_vehiculo = (updateReservationDto.tipo_vehiculo.ToLower()) switch
				{
					"autos" => "lugar_autos",
					"motos" => "lugar_motos",
					"camionetas" => "lugar_camionetas",
					"bicicletas" => "lugar_bicicletas",
					_ => throw new BadRequestException("el tipo de vehiculo no es valido"),
				};

				//hacemos el update correspondiente
				await _guardameLugarDacService.UpdateReservation(updateReservationDto);

			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}
		}

		public async Task<List<GetComentDto>> ComentByGarage(int garageId)
		{
			try
			{
				//valido que ID no sea nulo
				Throws.ThrowIfNotPositive(garageId, new BadRequestException("ID no puede ser negativo."));

				List<GetComentDto> getComentDto = await _guardameLugarDacService.ComentByGarage(garageId);

				return getComentDto;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}
		}

		public async Task<GetComentDto> ComentByReserva(int reservationId)
		{
			try
			{
				//valido que ID no sea nulo
				Throws.ThrowIfNotPositive(reservationId, new BadRequestException("ID no puede ser negativo."));

				GetComentDto getComentDto = await _guardameLugarDacService.ComentByReserva(reservationId);

				if (getComentDto == null)
				{
					GetComentDto getComent = new GetComentDto() { };
					return getComent;
				}

				return getComentDto;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}
		}

		public async Task ComentRegister(ComentDto comentDto)
		{
			try
			{
				//validacion de datos completos
				Throws.ThrowIfNull(comentDto, new BadRequestException("Los parametros son nulos."));
				Throws.ThrowIfNotPositive(comentDto.reserva_id, new BadRequestException("reserva esta vacio."));
				Throws.ThrowIfNull(comentDto.calificacion_1, new BadRequestException("calificacion 1 esta vacio."));
				Throws.ThrowIfNull(comentDto.calificacion_2, new BadRequestException("calificacion 2 esta vacio."));
				Throws.ThrowIfNull(comentDto.calificacion_3, new BadRequestException("calificacion 3 esta vacio."));
				Throws.ThrowIfNull(comentDto.calificacion_media, new BadRequestException("calificacion media esta vacio."));

				await _guardameLugarDacService.ComentRegister(comentDto);

			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}
		}
	}
}

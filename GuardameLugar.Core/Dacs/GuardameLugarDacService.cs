﻿using GuardameLugar.Common;
using GuardameLugar.DataAccess;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using GuardameLugar.DataAccess.Base;
using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using GuardameLugar.Common.Dto;
using System.Collections.Generic;

namespace GuardameLugar.Core.Dacs
{
	public interface IGuardameLugarDacService : ITransactionDacAsync
	{
		Task SaveUser(UserDto userDto);
		Task<bool> MailValidation(string mail);
		Task<LogInDto> LogInUser(LogInReqDto logInReqDto);
		Task GarageRegister(GarageRegisterDto garageRegisterDto);
		Task<List<LocalidadesDto>> Localidades();
		Task<List<LocalidadesDto>> LocalidadesPorGarages();
		Task<GarageByIdDto> GetGarageById(int garageId);
		Task<List<GarageDto>> GetGarageByUser(int userId);
		Task<List<GarageDto>> GetGarages(string query);
		Task UpdateGarage(UpdateGarageDto updateGarageDto);
		Task GarageReservation(GarageReservationDto garageReservationDto);
		Task<List<ReservationUserDto>> GarageReservationByUserId(int userId);
		Task<List<ReservationGarageDto>> GarageReservationByGarageId(int garageId);
		Task<int> SpaceValidator(string vehicle, int garageId);
		Task SpaceReservation(string vehicle, int garageId); 
		Task UpdateReservation(UpdateReservationDto updateReservationDto);
		Task<List<GetComentDto>> ComentByGarage(int garageId);
		Task<GetComentDto> ComentByReserva(int reservationId);
		Task ComentRegister(ComentDto comentDto);
		Task InsertToken(string token, int userId);
		Task<UserResetDto> GetUserData(string mail);
		Task<TokenValidationDto> TokenValidation(string token);
		Task UpdatePassword(UpdatePasswordDto updatePasswordDto);
	}

	public class GuardameLugarDacService : IGuardameLugarDacService, IDisposable
	{
		private readonly GuardameLugarDac _guardameLugarDac;

		public GuardameLugarDacService(ILogger<GuardameLugarDacService> logger, IConfiguration configuration)
		{
			string connectionString = configuration["ConnectionStrings:DefaultConnection"];
			_guardameLugarDac = new GuardameLugarDac(connectionString, logger);
		}

		public async Task SaveUser(UserDto userDto)
		{
			await _guardameLugarDac.SaveUser(userDto);
		}

		public async Task<bool> MailValidation(string mail)
		{
			bool result = await _guardameLugarDac.MailValidation(mail);
			return result;
		}

		public async Task<LogInDto> LogInUser(LogInReqDto logInReqDto)
		{
			return await _guardameLugarDac.LogInUser(logInReqDto);
		}

		public async Task GarageRegister(GarageRegisterDto garageRegisterDto)
		{
			await _guardameLugarDac.GarageRegister(garageRegisterDto);
		}

		public async Task<List<LocalidadesDto>> Localidades()
		{
			List<LocalidadesDto> listaLocalidades = await _guardameLugarDac.Localidades();
			return listaLocalidades;
		}

		public async Task<List<LocalidadesDto>> LocalidadesPorGarages()
		{
			List<LocalidadesDto> listaLocalidades = await _guardameLugarDac.LocalidadesPorGarages();
			return listaLocalidades;
		}

		public async Task<GarageByIdDto> GetGarageById(int garageId)
		{
			GarageByIdDto garageByIdDto = await _guardameLugarDac.GetGarageById(garageId);
			return garageByIdDto;
		}

		public async Task<List<GarageDto>> GetGarageByUser(int userId)
		{
			List<GarageDto> garageList = await _guardameLugarDac.GetGarageByUser(userId);
			return garageList;
		}
		public async Task<List<GarageDto>> GetGarages(string query)
		{
			List<GarageDto> garageList = await _guardameLugarDac.GetGarages(query);
			return garageList;
		}

		public async Task UpdateGarage(UpdateGarageDto updateGarageDto)
		{
			await _guardameLugarDac.UpdateGarage(updateGarageDto);
		}

		public async Task GarageReservation(GarageReservationDto garageReservationDto)
		{
			await _guardameLugarDac.GarageReservation(garageReservationDto);
		}

		public async Task<List<ReservationUserDto>> GarageReservationByUserId(int userId)
		{
			List<ReservationUserDto> garageList = await _guardameLugarDac.GarageReservationByUserId(userId);
			return garageList;
		}

		public async Task<List<ReservationGarageDto>> GarageReservationByGarageId(int garageId)
		{
			List<ReservationGarageDto> garageList = await _guardameLugarDac.GarageReservationByGarageId(garageId);
			return garageList;
		}

		public async Task<int> SpaceValidator(string vehicle, int garageId)
		{
			int disp = await _guardameLugarDac.SpaceValidator(vehicle, garageId);
			return disp;
		}

		public async Task SpaceReservation(string vehicle, int garageId)
		{
			await _guardameLugarDac.SpaceReservation(vehicle, garageId);
		}

		public async Task UpdateReservation(UpdateReservationDto updateReservationDto)
		{
			await _guardameLugarDac.UpdateReservation(updateReservationDto);
		}

		public async Task<List<GetComentDto>> ComentByGarage(int garageId)
		{
			List<GetComentDto> getComentDto = await _guardameLugarDac.ComentByGarage(garageId);
			return getComentDto;
		}
		public async Task<GetComentDto> ComentByReserva(int reservationId)
		{
			GetComentDto getComentDto = await _guardameLugarDac.ComentByReserva(reservationId);
			return getComentDto;
		}
		public async Task ComentRegister(ComentDto comentDto)
		{
			await _guardameLugarDac.ComentRegister(comentDto);
		}
		public async Task InsertToken(string token, int userId)
		{
			await _guardameLugarDac.InsertToken(token, userId);
		}
		public async Task<UserResetDto> GetUserData(string mail)
		{
			UserResetDto userResetDto = await _guardameLugarDac.GetUserData(mail);
			return userResetDto;
		}
		public async Task<TokenValidationDto> TokenValidation(string token)
		{
			TokenValidationDto tokenValidation = await _guardameLugarDac.TokenValidation(token);
			return tokenValidation;
		}
		public async Task UpdatePassword(UpdatePasswordDto updatePasswordDto)
		{
			await _guardameLugarDac.UpdatePassword(updatePasswordDto);
		}

		#region --ITransactionDACAsync--
		public async Task<SqlTransaction> BeginTransactionAsync()
		{
			return await _guardameLugarDac.BeginTransactionAsync();
		}

		public void BeginTransaction(SqlTransaction oTransaction)
		{
			throw new NotImplementedException();
		}

		public bool TransactionOpened()
		{
			return _guardameLugarDac.TransactionOpened();
		}

		public void Commit()
		{
			_guardameLugarDac.Commit();
		}

		public void Rollback()
		{
			_guardameLugarDac.Rollback();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			// Cleanup
		}

		#endregion
	}
}

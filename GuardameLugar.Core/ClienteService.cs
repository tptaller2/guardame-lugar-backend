﻿using GuardameLugar.Common;
using GuardameLugar.Common.Dto;
using GuardameLugar.Common.Exceptions;
using GuardameLugar.Common.Helpers;
using GuardameLugar.Core.Dacs;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RestSharp;
using System;
using System.Net;
using System.Reflection;
using System.Threading.Tasks;

namespace GuardameLugar.Core
{
	public interface IClienteService
	{
		Task SaveUser(UserDto userDto);
		Task<LogInDto> LogInUser(LogInReqDto logInReqDto);
		Task ResetPassword(ResetPasswordDto resetPasswordDto);
		Task<TokenValidationDto> TokenValidation(string token);
		Task UpdatePassword(UpdatePasswordDto updatePasswordDto);
	}
	public class ClienteService : IClienteService
	{
		private readonly IGuardameLugarDacService _guardameLugarDacService;
		private readonly ILogger<ClienteService> _logger;

		private readonly string SendGridWeb;
		private readonly string SendGridToken;

		public ClienteService(ILogger<ClienteService> logger, IGuardameLugarDacService guardameLugarDacService, IConfiguration configuration)
		{
			_logger = logger;
			_guardameLugarDacService = guardameLugarDacService;

			SendGridWeb = configuration.GetSection("Sendgrid:Web").Value;
			SendGridToken = configuration.GetSection("Sendgrid:Token").Value;
		}

		public async Task SaveUser(UserDto userDto)
		{
			try
			{
				//validacion de datos completos
				Throws.ThrowIfNull(userDto, new BadRequestException("Los parametros son nulos."));
				Throws.ThrowIfEmpty(userDto.apellido, new BadRequestException("Apellido esta vacio."));
				Throws.ThrowIfEmpty(userDto.contraseña, new BadRequestException("contraseña esta vacio."));
				Throws.ThrowIfEmpty(userDto.mail, new BadRequestException("mail esta vacio."));
				Throws.ThrowIfEmpty(userDto.nombre, new BadRequestException("nombre esta vacio."));
				Throws.ThrowIfNull(userDto.rol, new BadRequestException("rol esta vacio."));
				Throws.ThrowIfEmpty(userDto.telefono, new BadRequestException("telefono esta vacio."));

				//validar unico mail
				string mail = userDto.mail;
				bool mailvalidator = await _guardameLugarDacService.MailValidation(mail);

				if (mailvalidator)
				{
					throw new BadRequestException("Este mail ya fue registrado.");
				}

				//encriptacion
				userDto.contraseña = EncriptingPass.GetSHA256(userDto.contraseña);

				//DataBase
				await _guardameLugarDacService.SaveUser(userDto);

				//enviar el mail
				string json = "{\"personalizations\":[{\"to\":[{\"email\":\"" + userDto.mail + "\",\"name\":\"" + userDto.nombre + "\"}],\"dynamic_template_data\":{\"nombre\":\"" + userDto.nombre + "\",\"link\":\"" + userDto.link + "\"}}],\"from\":{\"email\":\"noreply@guardamelugar.com\",\"name\":\"Guardame Lugar\"},\"template_id\":\"d-fb0d8619ee2a4ba39f47f19a9458c33e\"}";
				var client = new RestClient(SendGridWeb);
				var request = new RestRequest(Method.POST);
				request.AddHeader("content-type", "application/json");
				request.AddHeader("authorization", SendGridToken);
				request.AddParameter("application/json", json, ParameterType.RequestBody);
				_ = client.Execute(request);

			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}

		public async Task<LogInDto> LogInUser(LogInReqDto logInReqDto)
		{
			try
			{
				Throws.ThrowIfEmpty(logInReqDto.user, new BadRequestException("user esta vacio."));
				Throws.ThrowIfEmpty(logInReqDto.password, new BadRequestException("Pasword esta vacio."));

				if (logInReqDto.password.Length <= 15)
				{
					logInReqDto.password = EncriptingPass.GetSHA256(logInReqDto.password);
				}

				var userData = await _guardameLugarDacService.LogInUser(logInReqDto);
				Throws.ThrowIfNull(userData, new NotFoundException($"No se encontro el usuario: {logInReqDto.user}."));

				return userData;

			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}
		}

		public async Task ResetPassword(ResetPasswordDto resetPasswordDto)
		{
			try
			{
				//delete despues insertar token en la db
				UserResetDto userResetDto = await _guardameLugarDacService.GetUserData(resetPasswordDto.mail);
				string token = resetPasswordDto.mail + DateTime.Now;
				token = EncriptingPass.GetSHA256(token);
				await _guardameLugarDacService.InsertToken(token, userResetDto.user_id);

				//agregar token al link ( ?token= asdasldkjasdk ) mail+date.Now()
				string link = resetPasswordDto.link + "?token="+token;

				string json = "{\"personalizations\":[{\"to\":[{\"email\":\""+resetPasswordDto.mail+"\",\"name\":\""+ userResetDto.nombre+"\"}],\"dynamic_template_data\":{\"nombre\":\""+ userResetDto.nombre+"\",\"link\":\""+link+"\"}}],\"from\":{\"email\":\"noreply@guardamelugar.com\",\"name\":\"Guardame Lugar\"},\"template_id\":\"d-946ba4cb010b4f07b94781a956e3fb65\"}";
				var client = new RestClient(SendGridWeb);
				var request = new RestRequest(Method.POST);
				request.AddHeader("content-type", "application/json");
				request.AddHeader("authorization", SendGridToken);
				request.AddParameter("application/json", json, ParameterType.RequestBody);
				IRestResponse response = client.Execute(request);

				if (response.StatusCode != HttpStatusCode.Accepted)
				{
					throw new BadRequestException("Se produjo un error, por favor intente nuevamente en un rato.");
				}

			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}

		}
		
		public async Task<TokenValidationDto> TokenValidation(string token)
		{
			
			try
			{
				//validacion de datos completos
				Throws.ThrowIfNull(token, new BadRequestException("El campo token esta vacio."));

				TokenValidationDto tokenValidation = await _guardameLugarDacService.TokenValidation(token);

				if (tokenValidation == null)
				{
					TokenValidationDto tokenValidationDto = new TokenValidationDto() { };
					tokenValidationDto.mail = "No se encontraron tokens";
					return tokenValidationDto;
				}

				return tokenValidation;
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw;
			}
		}

		public async Task UpdatePassword(UpdatePasswordDto updatePasswordDto)
		{
			//validacion de datos completos
			Throws.ThrowIfNull(updatePasswordDto, new BadRequestException("Los parametros son nulos."));
			Throws.ThrowIfNull(updatePasswordDto.user_id, new BadRequestException("User debe ser positivo."));
			Throws.ThrowIfEmpty(updatePasswordDto.password, new BadRequestException("contraseña esta vacio."));

			//encriptar password
			updatePasswordDto.password = EncriptingPass.GetSHA256(updatePasswordDto.password);

			//update password y delete from token table
			await _guardameLugarDacService.UpdatePassword(updatePasswordDto);

		}
	}
}

﻿using Xunit;
using Moq;
using Microsoft.Extensions.Logging;
using GuardameLugar.Core.Dacs;
using GuardameLugar.Common.Dto;
using Microsoft.Extensions.Configuration;
using System;
using GuardameLugar.Common.Exceptions;

namespace GuardameLugar.Core.Tests
{
	public class ClienteServiceTests
	{
		

		[Fact]
		public void LogInUserWithCorrectParameters()
		{
            // Arrange
            Mock<IGuardameLugarDacService> mockDacService = new Mock<IGuardameLugarDacService>();
			Mock < IConfiguration > mockIconfig = new Mock<IConfiguration>();
			Mock<ILogger<ClienteService>> mockLogger = new Mock<ILogger<ClienteService>>();

			LogInReqDto logInReqDto = new LogInReqDto()
			{
				user = "validmail@mail.com",
				password = "contraseña"
			};

			LogInDto logInDto = new LogInDto()
			{
				nombre = "nombre",
				apellido = "apellido",
				contraseña = "contraseña",
				mail = "validmail@mail.com",
				rol = 1,
				telefono = "12345678",
				user_id = 1
			};

			mockIconfig.Setup(x => x.GetSection(It.IsAny<String>())).Returns(new Mock<IConfigurationSection>().Object);

			mockDacService.Setup(x => x.LogInUser(It.IsAny<LogInReqDto>())).ReturnsAsync(logInDto);

			ClienteService _clienteService = new ClienteService(mockLogger.Object, mockDacService.Object, mockIconfig.Object);

            // Act
            LogInDto result = _clienteService.LogInUser(logInReqDto).Result;

			// Assert
			Assert.NotNull(result.contraseña);
        }

		[Fact()]
		public void LogInUserNotFound()
		{
			// Arrange
			Mock<IGuardameLugarDacService> mockDacService = new Mock<IGuardameLugarDacService>();
			Mock<IConfiguration> mockIconfig = new Mock<IConfiguration>();
			Mock<ILogger<ClienteService>> mockLogger = new Mock<ILogger<ClienteService>>();

			LogInReqDto logInReqDto = new LogInReqDto()
			{
				user = "validmail@mail.com",
				password = "contraseña"
			};

			LogInDto logInDto = null;

			mockIconfig.Setup(x => x.GetSection(It.IsAny<String>())).Returns(new Mock<IConfigurationSection>().Object);

			mockDacService.Setup(x => x.LogInUser(It.IsAny<LogInReqDto>())).ReturnsAsync(logInDto);

			ClienteService _clienteService = new ClienteService(mockLogger.Object, mockDacService.Object, mockIconfig.Object);

			// Act & Assert
			Assert.ThrowsAsync<NotFoundException>(() => _clienteService.LogInUser(logInReqDto));

		}		
	}
}
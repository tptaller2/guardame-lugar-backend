﻿using GuardameLugar.Common.Dto;
using GuardameLugar.Common.Exceptions;
using GuardameLugar.Core.Dacs;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace GuardameLugar.Core.Tests
{
	public class GarageServiceTests
	{
		[Fact]
		public void GarageRegisterWithCorrectParameters()
		{
			// Arrange
			Mock<IGuardameLugarDacService> mockDacService = new Mock<IGuardameLugarDacService>();
			Mock<ILogger<GarageService>> mockLogger = new Mock<ILogger<GarageService>>();

			GarageRegisterDto garageRegisterDto = new GarageRegisterDto()
			{
				user_id = 1,
				altura_maxima = 200,
				coordenadas = "1.2.3.4.74",
				direccion = "Av verdadera 123",
				localidad_garage = 2,
				lugar_autos = 3,
				lugar_bicicletas = 1,
				lugar_camionetas = 4,
				lugar_motos = 5,
				nombre_garage = "nombre",
				telefono = "123456"
			};

			mockDacService.Setup(x => x.GarageRegister(It.IsAny<GarageRegisterDto>()));

			GarageService _GarageService = new GarageService(mockLogger.Object, mockDacService.Object);

			// Act
			var result = _GarageService.GarageRegister(garageRegisterDto);

			// Assert
			Assert.NotNull(result);
		}

		[Fact]
		public void GarageRegisterWithIncorrectCoordenadasParameters()
		{
			// Arrange
			Mock<IGuardameLugarDacService> mockDacService = new Mock<IGuardameLugarDacService>();
			Mock<ILogger<GarageService>> mockLogger = new Mock<ILogger<GarageService>>();

			GarageRegisterDto garageRegisterDto = new GarageRegisterDto()
			{
				user_id = 1,
				altura_maxima = 200,
				coordenadas = "",
				direccion = "Av verdadera 123",
				localidad_garage = 2,
				lugar_autos = 3,
				lugar_bicicletas = 1,
				lugar_camionetas = 4,
				lugar_motos = 5,
				nombre_garage = "nombre",
				telefono = "123456"
			};

			mockDacService.Setup(x => x.GarageRegister(It.IsAny<GarageRegisterDto>()));

			GarageService _GarageService = new GarageService(mockLogger.Object, mockDacService.Object);

			// Act & Assert
			Assert.ThrowsAsync<BadRequestException>(() => _GarageService.GarageRegister(garageRegisterDto));
		}
	}
}
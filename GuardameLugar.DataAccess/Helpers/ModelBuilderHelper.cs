﻿

using GuardameLugar.Common.Dto;
using System;
using System.Data;

namespace GuardameLugar.DataAccess.Helpers
{
	internal static class ModelBuilderHelper
	{
		//aca van los mapeos de la base de datos

		internal static LogInDto BuildUserData(IDataReader reader)
		{
			LogInDto User = new LogInDto();
			User.user_id = int.Parse(reader["user_id"].ToString());
			User.nombre = (reader["nombre"].ToString());
			User.apellido = (reader["apellido"].ToString());
			User.mail = (reader["mail"].ToString());
			User.rol = int.Parse(reader["rol"].ToString());
			User.telefono = (reader["telefono"].ToString());
			User.contraseña = (reader["contraseña"].ToString());

			return User;
		}

		internal static LocalidadesDto BuildLocalidadesData(IDataReader reader)
		{
			LocalidadesDto localidades = new LocalidadesDto();
			localidades.localidad_id = int.Parse(reader["localidad_id"].ToString());
			localidades.nombre_localidad = (reader["nombre_localidad"].ToString());
			return localidades;
		}

		internal static GarageDto BuildGaragesData(IDataReader reader)
		{
			GarageDto garageDto = new GarageDto();
			garageDto.altura_maxima = int.Parse(reader["altura_maxima"].ToString());
			garageDto.coordenadas = (reader["coordenadas"].ToString());
			garageDto.direccion = (reader["direccion"].ToString());
			garageDto.garage_id = int.Parse(reader["garage_id"].ToString());
			garageDto.localidad_garage = int.Parse(reader["localidad_garage"].ToString());
			garageDto.nombre_localidad = (reader["nombre_localidad"].ToString());
			garageDto.lugar_autos = int.Parse(reader["lugar_autos"].ToString());
			garageDto.lugar_bicicletas = int.Parse(reader["lugar_bicicletas"].ToString());
			garageDto.lugar_camionetas = int.Parse(reader["lugar_camionetas"].ToString());
			garageDto.lugar_motos = int.Parse(reader["lugar_motos"].ToString());
			garageDto.nombre_garage = (reader["nombre_garage"].ToString());
			garageDto.telefono = (reader["telefono"].ToString());
			int.TryParse(reader["contador"].ToString(), out int contador);
			garageDto.contador = contador;
			double.TryParse(reader["promedio"].ToString(), out double promedio);
			garageDto.promedio = promedio;
			return garageDto;
		}

		internal static GarageByIdDto BuildGaragesByIdData(IDataReader reader)
		{
			GarageByIdDto garageByIdDto = new GarageByIdDto();
			garageByIdDto.altura_maxima = int.Parse(reader["altura_maxima"].ToString());
			garageByIdDto.coordenadas = (reader["coordenadas"].ToString());
			garageByIdDto.direccion = (reader["direccion"].ToString());
			garageByIdDto.garage_id = int.Parse(reader["garage_id"].ToString());
			garageByIdDto.localidad_garage = int.Parse(reader["localidad_garage"].ToString());
			garageByIdDto.nombre_localidad = (reader["nombre_localidad"].ToString());
			garageByIdDto.lugar_autos = int.Parse(reader["lugar_autos"].ToString());
			garageByIdDto.lugar_bicicletas = int.Parse(reader["lugar_bicicletas"].ToString());
			garageByIdDto.lugar_camionetas = int.Parse(reader["lugar_camionetas"].ToString());
			garageByIdDto.lugar_motos = int.Parse(reader["lugar_motos"].ToString());
			garageByIdDto.nombre_garage = (reader["nombre_garage"].ToString());
			garageByIdDto.telefono = (reader["telefono"].ToString());
			return garageByIdDto;
		}

		internal static ReservationGarageDto BuildReservationGarage(IDataReader reader)
		{
			ReservationGarageDto reservationGarageDto = new ReservationGarageDto();
			reservationGarageDto.estado = int.Parse(reader["estado"].ToString());
			reservationGarageDto.apellido = (reader["apellido"]).ToString();
			reservationGarageDto.mail = (reader["mail"]).ToString();
			reservationGarageDto.nombre = (reader["nombre"]).ToString();
			reservationGarageDto.horario_transaccion = DateTime.Parse(reader["horario_transaccion"].ToString());
			reservationGarageDto.nombre_garage = (reader["nombre_garage"]).ToString();
			reservationGarageDto.reserva_id = int.Parse(reader["reserva_id"].ToString());
			reservationGarageDto.telefono = (reader["telefono"]).ToString();
			reservationGarageDto.garage_id = int.Parse(reader["garage_id"].ToString());
			reservationGarageDto.tipo_vehiculo = (reader["tipo_vehiculo"].ToString());
			reservationGarageDto.direccion = (reader["direccion"].ToString());
			reservationGarageDto.nombre_localidad = (reader["nombre_localidad"].ToString());
			reservationGarageDto.descripcion = (reader["descripcion"].ToString());

			return reservationGarageDto;
		}

		internal static ReservationUserDto BuildReservationUser(IDataReader reader)
		{
			ReservationUserDto ReservationUserDto = new ReservationUserDto();
			ReservationUserDto.estado = int.Parse(reader["estado"].ToString());
			ReservationUserDto.direccion = (reader["direccion"]).ToString();
			ReservationUserDto.horario_transaccion = DateTime.Parse(reader["horario_transaccion"].ToString());
			ReservationUserDto.nombre_garage = (reader["nombre_garage"]).ToString();
			ReservationUserDto.reserva_id = int.Parse(reader["reserva_id"].ToString());
			ReservationUserDto.telefono = (reader["telefono"]).ToString();
			ReservationUserDto.nombre_localidad = (reader["nombre_localidad"]).ToString();
			ReservationUserDto.tipo_vehiculo = (reader["tipo_vehiculo"].ToString());
			ReservationUserDto.descripcion = (reader["descripcion"].ToString());
			ReservationUserDto.garage_id = int.Parse(reader["garage_id"].ToString());

			return ReservationUserDto;
		}

		internal static GetComentDto BuildComentById(IDataReader reader)
		{
			GetComentDto getComentDto = new GetComentDto();
			getComentDto.reserva_id = int.Parse(reader["reserva_id"].ToString());
			getComentDto.nombre = (reader["nombre"]).ToString();
			getComentDto.comentario_id = int.Parse(reader["comentario_id"].ToString());
			getComentDto.comentario = (reader["comentario"]).ToString();
			getComentDto.calificacion_1 = double.Parse(reader["calificacion_1"].ToString());
			getComentDto.calificacion_2 = double.Parse(reader["calificacion_2"].ToString());
			getComentDto.calificacion_3 = double.Parse(reader["calificacion_3"].ToString());
			getComentDto.calificacion_media = double.Parse(reader["calificacion_media"].ToString());

			return getComentDto;
		}

		internal static UserResetDto BuildUserResetData(IDataReader reader)
		{
			UserResetDto userResetDto = new UserResetDto();
			userResetDto.user_id = int.Parse(reader["user_id"].ToString());
			userResetDto.nombre = (reader["nombre"].ToString());
			userResetDto.mail = (reader["mail"].ToString());

			return userResetDto;
		}

		internal static TokenValidationDto BuildTokenData(IDataReader reader)
		{
			TokenValidationDto tokenValidationDto = new TokenValidationDto();
			tokenValidationDto.user_id = int.Parse(reader["user_id"].ToString());
			tokenValidationDto.mail = (reader["mail"].ToString());

			return tokenValidationDto;
		}
	}
}

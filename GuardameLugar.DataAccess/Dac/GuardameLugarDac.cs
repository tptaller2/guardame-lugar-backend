﻿using GuardameLugar.Common;
using Microsoft.Extensions.Logging;
using GuardameLugar.DataAccess.Base;
using GuardameLugar.DataAccess.Extensions;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using GuardameLugar.DataAccess.Helpers;
using GuardameLugar.Common.Dto;
using System.Collections.Generic;

namespace GuardameLugar.DataAccess
{
	public class GuardameLugarDac : BaseDacAsync
	{
		private static readonly string _classFullName = MethodBase.GetCurrentMethod().DeclaringType?.ToString();
		private readonly ILogger _logger;

		public GuardameLugarDac(string connectionString, ILogger logger) : base(connectionString)
		{
			try
			{
				_logger = logger ?? throw new ArgumentNullException(nameof(logger));

				base.OpenConnectionAsync().Wait();
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				throw new AggregateException(_classFullName + ".GuardameLugarDac(string connectionString) (ctor)", ex);
			}
		}

		#region Client
		public async Task SaveUser(UserDto userDto)
		{

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"insert into usuarios (nombre, apellido, telefono, mail, contraseña, rol) values (@nombre, @apellido, @telefono, @mail, @contraseña, @rol);";

					oCommand.AddParameter("nombre", DbType.String, userDto.nombre);
					oCommand.AddParameter("apellido", DbType.String, userDto.apellido);
					oCommand.AddParameter("telefono", DbType.String, userDto.telefono);
					oCommand.AddParameter("mail", DbType.String, userDto.mail);
					oCommand.AddParameter("contraseña", DbType.String, userDto.contraseña);
					oCommand.AddParameter("rol", DbType.Int32, userDto.rol);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".SaveUser(UserDto userDto)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<bool> MailValidation(string mail)
		{
			SqlDataReader reader;

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"select mail from usuarios where mail = @mail";

					oCommand.AddParameter("mail", DbType.String, mail);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "MailValidation");
					reader = oCommand.EndExecuteReader(asyncResult);

					if (reader.HasRows)
					{
						return true;
					}
					else
					{
						return false;
					}

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".MailValidation(string mail)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<LogInDto> LogInUser(LogInReqDto logInReqDto)
		{
			LogInDto objUser = null;
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"SELECT * FROM usuarios WHERE mail = @user and contraseña = @password";

					oCommand.AddParameter("user", DbType.String, logInReqDto.user);
					oCommand.AddParameter("password", DbType.String, logInReqDto.password);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "LogInUser");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						objUser = ModelBuilderHelper.BuildUserData(reader);
					}
					return objUser;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".LogInUser(string user, string password)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task InsertToken(string token, int userId)
		{

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"delete from token where user_id = @user_id;
											 insert into token (token, user_id) values (@token, @user_id);";

					oCommand.AddParameter("token", DbType.String, token);
					oCommand.AddParameter("user_id", DbType.Int32, userId);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".InsertToken(string token, int userId)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<TokenValidationDto> TokenValidation(string token)
		{
			SqlDataReader reader;
			TokenValidationDto tokenValidationDto = null;

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"select u.user_id,u.mail from token t inner join usuarios u on t.user_id = u.user_id where t.token = @token";

					oCommand.AddParameter("token", DbType.String, token);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "TokenValidation");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						tokenValidationDto = ModelBuilderHelper.BuildTokenData(reader);
					}
					return tokenValidationDto;

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".MailValidation(string mail)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task UpdatePassword(UpdatePasswordDto updatePasswordDto)
		{
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandText = @"Update usuarios set contraseña = @password where user_id = @user_id;
											 delete from token where user_id = @user_id;";

					oCommand.AddParameter("user_id", DbType.Int32, updatePasswordDto.user_id);
					oCommand.AddParameter("password", DbType.String, updatePasswordDto.password);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".UpdatePassword(UpdatePasswordDto updatePasswordDto)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<UserResetDto> GetUserData(string mail)
		{
			UserResetDto objUser = null;
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"SELECT * FROM usuarios WHERE mail = @mail";

					oCommand.AddParameter("mail", DbType.String, mail);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "LogInUser");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						objUser = ModelBuilderHelper.BuildUserResetData(reader);
					}
					return objUser;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".LogInUser(string user, string password)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		#endregion

		#region Garage
		public async Task GarageRegister(GarageRegisterDto garageRegisterDto)
		{

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"INSERT INTO GARAGES (altura_maxima, coordenadas, telefono, direccion, localidad_garage, lugar_autos, lugar_bicicletas, lugar_camionetas, lugar_motos, nombre_garage) 
                                             VALUES	(@altura_maxima, @coordenadas, @telefono, @direccion, @localidad_garage, @lugar_autos, @lugar_bicicletas, @lugar_camionetas, @lugar_motos, @nombre_garage);
											SELECT SCOPE_IDENTITY()";


					oCommand.AddParameter("altura_maxima", DbType.Decimal, garageRegisterDto.altura_maxima);
					oCommand.AddParameter("coordenadas", DbType.String, garageRegisterDto.coordenadas);
					oCommand.AddParameter("telefono", DbType.String, garageRegisterDto.telefono);
					oCommand.AddParameter("direccion", DbType.String, garageRegisterDto.direccion);
					oCommand.AddParameter("localidad_garage", DbType.Int32, garageRegisterDto.localidad_garage);
					oCommand.AddParameter("lugar_autos", DbType.Int32, garageRegisterDto.lugar_autos);
					oCommand.AddParameter("lugar_bicicletas", DbType.Int32, garageRegisterDto.lugar_bicicletas);
					oCommand.AddParameter("lugar_camionetas", DbType.Int32, garageRegisterDto.lugar_camionetas);
					oCommand.AddParameter("lugar_motos", DbType.Int32, garageRegisterDto.lugar_motos);
					oCommand.AddParameter("nombre_garage", DbType.String, garageRegisterDto.nombre_garage);

					int garage_id = Convert.ToInt32(oCommand.ExecuteScalar());

					oCommand.CommandText = @"INSERT INTO [dbo].[garage_por_usuario] (user_id, garage_id) VALUES 
											((select user_id from [dbo].[usuarios] where user_id = @user_id), (select [garage_id] from [dbo].[garages] where [garage_id] = @garage_id));";

					oCommand.AddParameter("user_id", DbType.Int32, garageRegisterDto.user_id);
					oCommand.AddParameter("garage_id", DbType.Int32, garage_id);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".ValidMainCard(long accountId)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<List<LocalidadesDto>> Localidades()
		{
			List<LocalidadesDto> listaLocalidades = new List<LocalidadesDto>();
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"SELECT * FROM localidades;";

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "localidades");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						listaLocalidades.Add(ModelBuilderHelper.BuildLocalidadesData(reader));
					}
					return listaLocalidades;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".Localidades()", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<List<LocalidadesDto>> LocalidadesPorGarages()
		{
			List<LocalidadesDto> listaLocalidades = new List<LocalidadesDto>();
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"select distinct l.localidad_id, l.nombre_localidad from localidades l inner join garages g on l.localidad_id = g.localidad_garage 
											where g.lugar_autos > 0 or g.lugar_motos> 0 or g.lugar_camionetas > 0 or g.lugar_bicicletas > 0
											order by localidad_id;";

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "localidades");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						listaLocalidades.Add(ModelBuilderHelper.BuildLocalidadesData(reader));
					}
					return listaLocalidades;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".Localidades()", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<GarageByIdDto> GetGarageById(int garageId)
		{
			GarageByIdDto garageByIdDto = null;
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"SELECT * FROM garages g inner join [guardameLugarDB].[dbo].localidades l on g.localidad_garage = l.localidad_id where garage_id = @garage_id order by l.nombre_localidad ;";

					oCommand.AddParameter("garage_id", DbType.Int32, garageId);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "GetGarageById");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						garageByIdDto = ModelBuilderHelper.BuildGaragesByIdData(reader);
					}
					return garageByIdDto;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".Localidades()", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<List<GarageDto>> GetGarageByUser(int userId)
		{
			List<GarageDto> garageList = new List<GarageDto>();
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"select top 50 g.*, l.nombre_localidad, count(cal.comentario_id) as contador, sum(cal.calificacion_media) as promedio from garages g 
											 inner join localidades l on g.localidad_garage = l.localidad_id
											 left join reservas r on g.garage_id = r.garage_id
											 left join comentarios_calificacion cal on cal.reserva_id = r.reserva_id
											 where g.garage_id in (select garage_id from garage_por_usuario where user_id = @user_id)
											 group by  g.garage_id, g.nombre_garage, g.direccion, g.coordenadas, g.localidad_garage , l.nombre_localidad, g.telefono, g.lugar_autos, g.lugar_motos,
											 g.lugar_camionetas, g.lugar_bicicletas, g.altura_maxima
											 order by g.localidad_garage;";

					oCommand.AddParameter("user_id", DbType.Int32, userId);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "GetGarageByUser");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						garageList.Add(ModelBuilderHelper.BuildGaragesData(reader));
					}
					return garageList;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".GetGarageByUser(int userId)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<List<GarageDto>> GetGarages(string query)
		{
			List<GarageDto> garageList = new List<GarageDto>();
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = query;

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "GetGarages");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						garageList.Add(ModelBuilderHelper.BuildGaragesData(reader));
					}
					return garageList;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".GetGarages(string query)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task UpdateGarage(UpdateGarageDto updateGarageDto)
		{

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandText = @"Update garages set [nombre_garage] = @nombre_garage, [direccion] = @direccion, [coordenadas] = @coordenadas, [localidad_garage] = @localidad_garage,
											[telefono] = @telefono, [lugar_autos] = @lugar_autos, [lugar_motos] = @lugar_motos, [lugar_camionetas] = @lugar_camionetas,
											[lugar_bicicletas] = @lugar_bicicletas, [altura_maxima] = @altura_maxima where [garage_id] = @garage_id;";

					oCommand.AddParameter("garage_id", DbType.Int32, updateGarageDto.garage_id);
					oCommand.AddParameter("altura_maxima", DbType.Decimal, updateGarageDto.altura_maxima);
					oCommand.AddParameter("coordenadas", DbType.String, updateGarageDto.coordenadas);
					oCommand.AddParameter("telefono", DbType.String, updateGarageDto.telefono);
					oCommand.AddParameter("direccion", DbType.String, updateGarageDto.direccion);
					oCommand.AddParameter("localidad_garage", DbType.Int32, updateGarageDto.localidad_garage);
					oCommand.AddParameter("lugar_autos", DbType.Int32, updateGarageDto.lugar_autos);
					oCommand.AddParameter("lugar_bicicletas", DbType.Int32, updateGarageDto.lugar_bicicletas);
					oCommand.AddParameter("lugar_camionetas", DbType.Int32, updateGarageDto.lugar_camionetas);
					oCommand.AddParameter("lugar_motos", DbType.Int32, updateGarageDto.lugar_motos);
					oCommand.AddParameter("nombre_garage", DbType.String, updateGarageDto.nombre_garage);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".UpdateGarage(GarageDto garageDto)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}
		#endregion

		#region Reservation

		public async Task GarageReservation(GarageReservationDto garageReservationDto)
		{
			var tzInfo = TimeZoneInfo.FindSystemTimeZoneById("Argentina Standard Time");
			DateTime localDateTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, tzInfo);

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"INSERT INTO RESERVAS (user_id, garage_id, horario_transaccion, tipo_vehiculo, estado) VALUES (@user_id, @garage_id, @fecha, @tipo_vehiculo, @estado);";


					oCommand.AddParameter("user_id", DbType.Int32, garageReservationDto.user_id);
					oCommand.AddParameter("garage_id", DbType.Int32, garageReservationDto.garage_id);
					oCommand.AddParameter("fecha", DbType.DateTime, localDateTime);
					oCommand.AddParameter("tipo_vehiculo", DbType.String, garageReservationDto.tipo_vehiculo);
					oCommand.AddParameter("estado", DbType.Int32, 1);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".GarageReservation(GarageReservationDto garageReservationDto)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<List<ReservationUserDto>> GarageReservationByUserId(int userId)
		{
			List<ReservationUserDto> ReservationUserDto = new List<ReservationUserDto>();
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"SELECT reserva_id, r.horario_transaccion, r.estado, r.tipo_vehiculo, g.nombre_garage, g.direccion, l.nombre_localidad, g.telefono, e.descripcion, g.garage_id
											 FROM reservas r INNER JOIN garages g ON r.garage_id = g.garage_id INNER JOIN localidades l
											 ON l.localidad_id = g.localidad_garage INNER JOIN estado_reservas e ON estado_id = r.estado
											 WHERE r.user_id = @user_id ORDER BY horario_transaccion DESC;";

					oCommand.AddParameter("user_id", DbType.Int32, userId);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "GetReservationByUser");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						ReservationUserDto.Add(ModelBuilderHelper.BuildReservationUser(reader));
					}
					return ReservationUserDto;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".GetGarageByUser(int userId)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<List<ReservationGarageDto>> GarageReservationByGarageId(int garageId)
		{
			List<ReservationGarageDto> ReservationGarageDto = new List<ReservationGarageDto>();
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"SELECT reserva_id, r.horario_transaccion, r.estado, r.tipo_vehiculo,
											 u.nombre, u.apellido, u.telefono, u.mail , g.nombre_garage, g.garage_id, g.direccion, l.nombre_localidad, e.descripcion 
											 FROM reservas r INNER JOIN usuarios u ON r.user_id = u.user_id INNER JOIN garages g ON r.garage_id = g.garage_id
											 INNER JOIN localidades l ON l.localidad_id = g.localidad_garage INNER JOIN estado_reservas e ON estado_id = r.estado
											 WHERE r.garage_id = @garage_id ORDER BY horario_transaccion DESC";

					oCommand.AddParameter("garage_id", DbType.Int32, garageId);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "GetReservationByGarage");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						ReservationGarageDto.Add(ModelBuilderHelper.BuildReservationGarage(reader));
					}
					return ReservationGarageDto;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".GetReservationByGarage(int garageId)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<int> SpaceValidator(string vehicle, int garageId)
		{
			int disp = 0;
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = $"select {vehicle} from garages where garage_id = @garage_id;";

					oCommand.AddParameter("garage_id", DbType.Int32, garageId);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "SpaceValidator");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						disp = int.Parse(reader[$"{vehicle}"].ToString());
					}
					return disp;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".GetGarageByUser(int userId)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task SpaceReservation(string vehicle, int garageId)
		{
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandText = $"UPDATE garages SET {vehicle} = ({vehicle} - 1) WHERE garage_id = @garage_id;";

					oCommand.AddParameter("garage_id", DbType.Int32, garageId);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".UpdateGarage(GarageDto garageDto)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task UpdateReservation(UpdateReservationDto updateReservationDto)
		{
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandText = @$"UPDATE reservas SET estado = @estado WHERE reserva_id = @reserva_id;
						UPDATE garages set {updateReservationDto.tipo_vehiculo} = {updateReservationDto.tipo_vehiculo} + 1 
						WHERE garage_id = @garage_id and (select estado from reservas where reserva_id = @reserva_id) not in ('1','4');";


					oCommand.AddParameter("estado", DbType.Int32, updateReservationDto.estado);
					oCommand.AddParameter("reserva_id", DbType.Int32, updateReservationDto.reserva_id);
					oCommand.AddParameter("garage_id", DbType.Int32, updateReservationDto.garage_id);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".UpdateReservation(UpdateReservationDto updateReservationDto)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task ComentRegister(ComentDto comentDto)
		{

			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"insert into comentarios_calificacion (reserva_id, comentario, calificacion_1, calificacion_2, calificacion_3, calificacion_media) 
											 values (@reserva_id, @comentario, @calificacion_1, @calificacion_2, @calificacion_3, @calificacion_media);";


					oCommand.Parameters.AddWithValue("@reserva_id", comentDto.reserva_id);
					oCommand.Parameters.AddWithValue("@comentario", comentDto.comentario);
					oCommand.Parameters.AddWithValue("@calificacion_1", comentDto.calificacion_1);
					oCommand.Parameters.AddWithValue("@calificacion_2", comentDto.calificacion_2);
					oCommand.Parameters.AddWithValue("@calificacion_3", comentDto.calificacion_3);
					oCommand.Parameters.AddWithValue("@calificacion_media", comentDto.calificacion_media);

					await oCommand.ExecuteNonQueryAsync();

				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".ComentRegister(ComentDto comentDto)", ex);
				}
				finally
				{
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<List<GetComentDto>> ComentByGarage(int garageId)
		{
			List<GetComentDto> getComentDto = new List<GetComentDto>();
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"select com.*, u.nombre from comentarios_calificacion com
											 inner join reservas r on com.reserva_id = r.reserva_id
											 inner join usuarios u on r.user_id = u.user_id
											 where r.garage_id = @garage_id;";

					oCommand.AddParameter("garage_id", DbType.Int32, garageId);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "ComentByGarage");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						getComentDto.Add(ModelBuilderHelper.BuildComentById(reader));
					}
					return getComentDto;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".ComentByGarage(int garageId)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		public async Task<GetComentDto> ComentByReserva(int reservaId)
		{
			GetComentDto getComentDto = null;
			SqlDataReader reader = null;
			using (SqlCommand oCommand = await base.GetCommandAsync())
			{
				try
				{
					oCommand.CommandType = CommandType.Text;
					oCommand.CommandText = @"select com.*, u.nombre from comentarios_calificacion com
											 inner join reservas r on com.reserva_id = r.reserva_id
											 inner join usuarios u on r.user_id = u.user_id
											 where com.reserva_id = @reserva_id;";

					oCommand.AddParameter("reserva_id", DbType.Int32, reservaId);

					IAsyncResult asyncResult = ExecuteAsync(oCommand, "ComentByReserva");
					reader = oCommand.EndExecuteReader(asyncResult);

					while (await reader.ReadAsync())
					{
						getComentDto = ModelBuilderHelper.BuildComentById(reader);
					}
					return getComentDto;
				}
				catch (Exception ex)
				{
					_logger.LogError(ex, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException(_classFullName + ".ComentByReserva(int reservaId)", ex);
				}
				finally
				{
					if (reader != null && !reader.IsClosed)
						reader.Close();
					if (!TransactionOpened())
						base.CloseCommand();
				}
			}
		}

		#endregion

		#region ExecuteParameters
		private IAsyncResult ExecuteAsync(SqlCommand oCommand, string methods = "*")
		{
			var watch = System.Diagnostics.Stopwatch.StartNew();
			int count = 0;
			IAsyncResult asyncResult = oCommand.BeginExecuteReader();
			while (!asyncResult.IsCompleted)
			{
				Thread.Sleep(100);
				count++;
				if (count == 1000000)
				{
					watch.Stop();
					var elapsedTime = watch.ElapsedMilliseconds;
					_logger.LogInformation("The method '" + methods + "' delay " + elapsedTime.ToString() + " miliseconds.", GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
					throw new AggregateException("The method '" + methods + "' it took longer than parameterized.");
				}
			}
			return asyncResult;
		}
		#endregion

	}
}




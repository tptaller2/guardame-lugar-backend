# Changelog

**Notas de Versión**

### Versión 1.2 - Release 7

**15/08/2020**

### Cambios:
- Signup - sistema envia correo electronico de bienvenida
- Ajustes por cambio de DB
- Refactor credenciales en config.json

#### Añadido:
- Reset password con envio de mail
- integracion con twilio

### Versión 1.1 - Release 6

**01/08/2020**

### Cambios:
- Refactor getGarage
- Refactor GetReservas

#### Añadido:
- Post Calificacion/comentario
- Put tabla calificaciones

### Versión 1.0 - Release 5

**19/07/2020**

### Cambios:
- Refactor general

#### Añadido:
- Put reservas para usuarios y clientes
- Unit Test
	
### Versión 0.4 - Release 4

**30/06/2020**

### Cambios:
- get localidades (solo con lugares disponibles)
- Mejora login
- Put garage
	
#### Añadido:
- Get reservas por usuario
- Get reservas por cliente
- Post reservas

### Versión 0.3 - Release 3

**18/06/2020**

### Cambios:
- Cors policy
- Refactor objetos
- Bug fixed
- Agregado Changelog.md

#### Añadido:
- Get garages por clientes con o sin filtros
- Put garage para modificacion

### Versión 0.2 - Release 2

**13/05/2020**

### Cambios:
- Password encriptada

#### Añadido:
- Post Registro de usuarios
- Post Registro/Carga de garage
- Get localidades
- Swagger documentation

### Versión 0.1 - Release 1

**29/04/2020**

### Cambios:

#### Añadido:
- Login
- Validaciones
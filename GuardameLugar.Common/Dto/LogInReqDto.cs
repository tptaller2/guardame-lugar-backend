﻿
namespace GuardameLugar.Common.Dto
{
	public class LogInReqDto
	{
		public string user { get; set; }
		public string password { get; set; }
	}
}

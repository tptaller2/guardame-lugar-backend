﻿
namespace GuardameLugar.Common.Dto
{
	public class ComentDto
	{
		public int reserva_id { get; set; }
		public string comentario { get; set; }
		public double calificacion_1 { get; set; }
		public double calificacion_2 { get; set; }
		public double calificacion_3 { get; set; }
		public double calificacion_media { get; set; }
	}
}

﻿
namespace GuardameLugar.Common.Dto
{
	public class UserResetDto
	{
		public int user_id { get; set; }
		public string nombre { get; set; }
		public string mail { get; set; }
	}
}

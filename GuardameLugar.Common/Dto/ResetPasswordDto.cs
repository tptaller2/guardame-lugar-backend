﻿
namespace GuardameLugar.Common.Dto
{
	public class ResetPasswordDto
	{
		public string mail { get; set; }
		public string link { get; set; }
	}
}

﻿
namespace GuardameLugar.Common.Dto
{
	public class GetComentDto
	{
		public int comentario_id { get; set; }
		public int reserva_id { get; set; }
		public string comentario { get; set; }
		public double calificacion_1 { get; set; }
		public double calificacion_2 { get; set; }
		public double calificacion_3 { get; set; }
		public double calificacion_media { get; set; }
		public string nombre { get; set; }

	}
}

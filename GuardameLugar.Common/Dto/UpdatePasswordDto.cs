﻿
namespace GuardameLugar.Common.Dto
{
	public class UpdatePasswordDto
	{
		public int user_id { get; set; }
		public string password { get; set; }
	}
}

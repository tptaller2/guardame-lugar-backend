﻿using System;

namespace GuardameLugar.Common.Dto
{
	public class ReservationUserDto
	{
		public int reserva_id { get; set; }
		public DateTime horario_transaccion { get; set; }
		public int estado { get; set; }
		public string nombre_garage { get; set; }
		public string direccion { get; set; }
		public string nombre_localidad { get; set; }
		public string telefono { get; set; }
		public string tipo_vehiculo { get; set; }
		public string descripcion { get; set; }
		public int garage_id { get; set; }
	}
}

﻿
namespace GuardameLugar.Common.Dto
{
	public class GarageReservationDto
	{
		public int user_id { get; set; }
		public int garage_id { get; set; }
		public string tipo_vehiculo { get; set; }
	}
}

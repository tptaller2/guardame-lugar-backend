﻿
namespace GuardameLugar.Common.Dto
{
	public class UpdateReservationDto
	{
		public int reserva_id { get; set; }
		public int estado { get; set; }
		public int garage_id { get; set; }
		public string tipo_vehiculo { get; set; }
	}
}

﻿using GuardameLugar.Core;
using System.Threading.Tasks;
using GuardameLugar.Common;
using System;
using Microsoft.Extensions.Logging;
using System.Reflection;
using Microsoft.AspNetCore.Http;
using GuardameLugar.Common.Extensions;
using GuardameLugar.Common.Exceptions;
using GuardameLugar.Common.Helpers;
using System.Net.Mime;
using GuardameLugar.Common.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;

namespace GuardameLugar.API.Controllers
{
    [Route("guardamelugar/[controller]")]
    [ApiController]
    [EnableCors("AllowOrigin")]
    [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(JsonMessage))]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(JsonMessage))]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity, Type = typeof(JsonMessage))]
    [Consumes(MediaTypeNames.Application.Json)]
    public class ClientesController : ControllerBase
    {
        private readonly IClienteService _clientesService;
        private readonly ILogger<ClientesController> _logger;

        public ClientesController(IClienteService clientesService, ILogger<ClientesController> logger)
        {
            _clientesService = clientesService;
            _logger = logger;
        }

        /// <summary>
        /// metodo para guardar un usuario
        /// </summary>
        /// <param name="userDto"></param>
        /// <returns></returns>
        [HttpPost("signup")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> SaveUser(UserDto userDto)
        
        {
            try
            {
                await _clientesService.SaveUser(userDto);
                return Response.Ok();
            }
            catch (BaseException e)
            {
                _logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
                return Response.HandleExceptions(e);
            }
            catch (Exception e)
            {
                _logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
                return Response.InternalServerError();
            }
        }

        [HttpPost("login")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LogInDto))]
        public async Task<ActionResult> LogInUser(LogInReqDto logInReqDto)
        {
            try
            {
                LogInDto objUser = await _clientesService.LogInUser(logInReqDto);
                return Response.Ok(objUser);
            }
            catch (BaseException e)
            {
                _logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
                return Response.HandleExceptions(e);
            }
            catch (Exception e)
            {
                _logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
                return Response.InternalServerError();
            }
        }

        [HttpPost("ResetPassword")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> ResetPassword(ResetPasswordDto resetPasswordDto)
        {
            try
            {
                await _clientesService.ResetPassword(resetPasswordDto);
                return Response.Ok();
            }
            catch (BaseException e)
            {
                _logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
                return Response.HandleExceptions(e);
            }
            catch (Exception e)
            {
                _logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
                return Response.InternalServerError();
            }
        }

        [HttpGet("TokenValidation/{token}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> TokenValidation(string token)
        {
            try
            {
                TokenValidationDto validToken = await _clientesService.TokenValidation(token);
                return Response.Ok(validToken);
            }
            catch (BaseException e)
            {
                _logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
                return Response.HandleExceptions(e);
            }
            catch (Exception e)
            {
                _logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
                return Response.InternalServerError();
            }
        }

        [HttpPatch("UpdatePassword")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult> UpdatePassword(UpdatePasswordDto updatePasswordDto)
        {
            try
            {
                await _clientesService.UpdatePassword(updatePasswordDto);
                return Response.Ok();
            }
            catch (BaseException e)
            {
                _logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
                return Response.HandleExceptions(e);
            }
            catch (Exception e)
            {
                _logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
                return Response.InternalServerError();
            }
        }
    }
}
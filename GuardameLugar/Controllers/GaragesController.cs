﻿using GuardameLugar.Common.Dto;
using GuardameLugar.Common.Exceptions;
using GuardameLugar.Common.Extensions;
using GuardameLugar.Common.Helpers;
using GuardameLugar.Core;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Reflection;
using System.Threading.Tasks;

namespace GuardameLugar.API.Controllers
{
	[Route("guardamelugar/[controller]")]
	[ApiController]
	[EnableCors("AllowOrigin")]
	[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(JsonMessage))]
	[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(JsonMessage))]
	[ProducesResponseType(StatusCodes.Status422UnprocessableEntity, Type = typeof(JsonMessage))]
	[Consumes(MediaTypeNames.Application.Json)]
	public class GaragesController : ControllerBase
	{
		private readonly ILogger<GaragesController> _logger;
		private readonly IGarageService _garageService;
		public GaragesController(ILogger<GaragesController> logger, IGarageService garageService)
		{
			_logger = logger;
			_garageService = garageService;
		}

		[HttpPost("GarageRegister")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JsonMessage))]
		public async Task<ActionResult> GarageRegister(GarageRegisterDto garageRegisterDto)
		{
			try
			{
				await _garageService.GarageRegister(garageRegisterDto);
				return Response.Ok();
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("Localidades")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LocalidadesDto))]
		public async Task<ActionResult> Localidades()
		{
			try
			{
				List<LocalidadesDto> listaLocalidades = await _garageService.Localidades();
				return Response.Ok(listaLocalidades);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("Localidades/garages")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(LocalidadesDto))]
		public async Task<ActionResult> LocalidadesPorGarages()
		{
			try
			{
				List<LocalidadesDto> listaLocalidades = await _garageService.LocalidadesPorGarages();
				return Response.Ok(listaLocalidades);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("{garageId}")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GarageByIdDto))]
		public async Task<ActionResult> GetGarageById(int garageId)
		{
			try
			{
				GarageByIdDto garageByIdDto = await _garageService.GetGarageById(garageId);
				return Response.Ok(garageByIdDto);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("user/{userId}")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GarageDto))]
		public async Task<ActionResult> GetGarageByUser(int userId)
		{
			try
			{
				List<GarageDto> garageList = await _garageService.GetGarageByUser(userId);
				return Response.Ok(garageList);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GarageDto))]
		public async Task<ActionResult> GetGarages([FromQuery]string vehiculo, [FromQuery]int? localidad)
		{
			try
			{
				List<GarageDto> garageList = await _garageService.GetGarages(vehiculo, localidad);
				return Response.Ok(garageList);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpPut]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JsonMessage))]
		public async Task<ActionResult> UpdateGarage(UpdateGarageDto updateGarageDto)
		{
			try
			{
				await _garageService.UpdateGarage(updateGarageDto);
				return Response.Ok();
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpPost("reservation")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JsonMessage))]
		public async Task<ActionResult> GarageReservation(GarageReservationDto garageReservationDto)
		{
			try
			{
				await _garageService.GarageReservation(garageReservationDto);
				return Response.Ok();
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("reservation/user/{userId}")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ReservationUserDto))]
		public async Task<ActionResult> GarageReservationByUserId(int userId)
		{
			try
			{
				List<ReservationUserDto> garagesByUser = await _garageService.GarageReservationByUserId(userId);
				return Response.Ok(garagesByUser);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("reservation/garage/{garageId}")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ReservationGarageDto))]
		public async Task<ActionResult> GarageReservationByGarageId(int garageId)
		{
			try
			{
				List<ReservationGarageDto> garagesByGarageId = await _garageService.GarageReservationByGarageId(garageId);
				return Response.Ok(garagesByGarageId);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpPatch]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JsonMessage))]
		public async Task<ActionResult> UpdateReservation(UpdateReservationDto updateReservationDto)
		{
			try
			{
				await _garageService.UpdateReservation(updateReservationDto);
				return Response.Ok();
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("Coment/garage/{garageId}")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetComentDto))]
		public async Task<ActionResult> ComentByGarage(int garageId)
		{
			try
			{
				List<GetComentDto> getComentDto = await _garageService.ComentByGarage(garageId);
				return Response.Ok(getComentDto);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpGet("Coment/reservation/{reservationId}")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(GetComentDto))]
		public async Task<ActionResult> ComentByReserva(int reservationId)
		{
			try
			{
				GetComentDto getComentDto = await _garageService.ComentByReserva(reservationId);
				return Response.Ok(getComentDto);
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}

		[HttpPost("Coment")]
		[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(JsonMessage))]
		public async Task<ActionResult> ComentRegister(ComentDto comentDto)
		{
			try
			{
				await _garageService.ComentRegister(comentDto);
				return Response.Ok();
			}
			catch (BaseException e)
			{
				_logger.LogInformation(ExceptionHandlerHelper.ExceptionMessageStringToLogger(e));
				return Response.HandleExceptions(e);
			}
			catch (Exception e)
			{
				_logger.LogError(e, GetType().Name + "." + MethodBase.GetCurrentMethod().Name);
				return Response.InternalServerError();
			}
		}
	}
}
